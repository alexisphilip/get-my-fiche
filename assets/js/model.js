
/**
 * Model class.
 * Manages the data from JSON.
 */

class Model
{
    /**
     * Model constructor.
     */
    constructor()
    {
        this.data_rfc;
        this.data_home;
        this.init();
    }

    /**
     * Initialization method.
     */
    init()
    {
        this.getDataRFC();
        this.getDataHome();
    }

    /*--- GETTERS ---*/

    /**
     * Gets the JSON data from the URL below.
     * http://get-my-rfc.alexisphilip.fr/assets/data_rfc.json
     * The JSON data contains 3 RFC scores, as well as split information such as date, reference, name, etc.
     */
    getDataRFC()
    {
        let xhttp = new XMLHttpRequest();
        let self = this; // See usage below.
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                self.setDataRFC(JSON.parse(this.responseText)); // Usage of self because ".bind(this)" does not work with nested functions.
            }
        };
        xhttp.open("GET", "http://get-my-rfc.alexisphilip.fr/assets/data_rfc.json", true);
        xhttp.send();
    }

    /**
     * Gets the JSON data from the URL below.
     * http://get-my-rfc.alexisphilip.fr/assets/data_home.json
     * The JSON data contains 2 information, the title and the content.
     */
    getDataHome()
    {
        let xhttp = new XMLHttpRequest();
        let self = this; // See usage below.
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                self.setDataHome(JSON.parse(this.responseText)); // Usage of self because ".bind(this)" does not work with nested functions.
            }
        };
        xhttp.open("GET", "http://get-my-rfc.alexisphilip.fr/assets/data_home.json", true);
        xhttp.send();
    }

    /**
     * Returns the results.
     * Works as a "filter" for the results.
     * @param {String} search - String input.
     * @returns {Array} Contains the RFC scores which match the string input.
     */
    getResults(search)
    {
        // Will contain the RFC matching the search input string.
        let results = [];

        // If the search input is not only spaces or empty.
        if (search === " " || search.length === 0)
            // Returns an empty list.
            return results;

        // Lowercase the search string to search results.
        search = search.toLowerCase();

        // For each RFC score.
        for (let rfc_key in this.data_rfc) {

            // Current RFC score.
            let rfc = this.data_rfc[rfc_key];

            // If it matches the reference.
            if (("rfc" + rfc.ref).toLowerCase().includes(search)) {
                results.push(rfc);
            }
            // If it matches the name.
            else if (rfc.name.toLowerCase().includes(search)) {
                results.push(rfc);
            }
            // If it matches the description.
            else if (rfc.intro.toLowerCase().includes(search)) {
                results.push(rfc);
            }
            // If it matches the date.
            else if (rfc.date.toLowerCase().includes(search)) {
                results.push(rfc);
            }
            // If it matches one of the authors.
            else {
                // For each authors of the current score.
                for (let i = 0; i < rfc.authors.length; i++) {
                    if (rfc.authors[i].toLowerCase().includes(search)) {
                        results.push(rfc);
                        break;
                    }
                }
            }
        }

        log(results)

        return results;
    }

    /**
     * Gets a specific RFC score from a reference.
     * @param {String} ref - RFC's reference.
     * @returns {Object} RFC score object.
     */
    getRfcScoreData(ref)
    {
        for (let rfc_key in this.data_rfc) {

            if (this.data_rfc[rfc_key].ref === ref) {
                return this.data_rfc[rfc_key];
            }
        }

        log("Could not find RFC reference from variable 'data' in Model.getFiche()");
    }

    /**
     * Gets the home data.
     * @returns {Object} The home data object.
     */
    getHomeData()
    {
        return this.data_home;
    }

    /*--- SETTERS ---*/

    /**
     * Sets the RFC scores data.
     * @param {Object} data - RFC scores data object.
     */
    setDataRFC(data)
    {
        this.data_rfc = data;
    }

    /**
     * Sets the home data.
     * @param {Object} data - Home data object.
     */
    setDataHome(data)
    {
        this.data_home = data;
    }
}