
/**
 * App class.
 * Main JavaScript class, instantiate the other classes and start the app.
 */

class App
{
    /**
     * App constructor.
     */
    constructor()
    {
        // Instantiation of the other classes.
        this.model = new Model();
        this.dropDown = new DropDown();

        // DOM selectors.
        this.search_input = document.querySelector("input"); // Search input.
        this.btn_home = document.querySelector("header h1");

        // Columns.
        this.col_left = document.querySelector(".column-1");
        this.col_right = document.querySelector(".column-2");

        // Insertion zones.
        this.col_1_text = document.querySelector(".column-1 .text");
        this.col_2_text = document.querySelector(".column-2 .text");
        this.col_1_block = document.querySelector(".column-1 .block");
        this.col_2_block = document.querySelector(".column-2 .block");
    }

    /**
     * Initialization method.
     */
    init()
    {
        this.listener();
    }

    /**
     * Starts multiple listeners for different events.
     * Routes the user queries to their methods.
     */
    listener()
    {
        // "focus" on the search input.
        this.search_input.addEventListener("focus", e => {
            this.dropDown.setDropDown(true);
        });

        // "focusout" on the search input.
        this.search_input.addEventListener("focusout", e => {
            this.dropDown.setDropDown(false);
        });

        // "keyup" on the search input.
        this.search_input.addEventListener("keyup", e => {
            // Contains the RFC scores matching the input.
            let rfc_results = this.model.getResults(e.target.value);
            // Will append the RFC scores to the DOM if rfc_results is not empty.
            this.dropDown.resultManager(rfc_results);
        });

        // "mousedown" on document.
        document.addEventListener("mousedown", e => {
            // If the element is the <parent> from the drop down list or is one of its children.
            if (e.target.classList.contains("dp-element") || e.target.closest('.dp-element')) {

                var element;

                // If the element is <parent>
                if (e.target.classList.contains("dp-element")) {
                    element = e.target;
                }
                // If it is one of its children.
                else {
                    element = e.target.closest('.dp-element');
                }

                this.appendScore(element.dataset.ref);
            }
        });

        this.btn_home.addEventListener("mousedown", e => {
            this.appendHome();
        });
    }

    /**
     * Appends to the DOM a specific RFC score.
     * @param {Object} ref - Object containing RFC score data.
     */
    appendScore(ref)
    {
        let data_rfc = this.model.getRfcScoreData(ref),
            authors = "";

        this.columnPosition(1);

        this.col_1_text.innerHTML = data_rfc.name;
        this.col_1_block.innerHTML = data_rfc.content;

        for (let i = 0; i < data_rfc.authors.length; i++) {
            log(data_rfc.authors[i])
            authors += '<p>' + data_rfc.authors[i] + '</p>';
        }

        this.col_2_text.innerHTML = "Information";
        this.col_2_block.innerHTML = '\
            <h3>Reference</h3>\
            <p>RFC ' + data_rfc.ref + '</p>\
            <h3>Publication date</h3>\
            <p>' + data_rfc.date + '</p>\
            <h3>Author' + (data_rfc.authors.length > 1 ? "s" : "") + '</h3>\
            ' + authors + '\
        ';
    }

    /**
     * Appends to the DOM the home data.
     */
    appendHome()
    {
        let data_home = this.model.getHomeData();

        this.columnPosition(0);

        this.col_1_text.innerHTML = data_home[0].title;
        this.col_1_block.innerHTML = data_home[0].content;

        this.col_2_text.innerHTML = data_home[1].title;
        this.col_2_block.innerHTML = data_home[1].content;
    }

    /**
     * Sets the column position.
     * @param {Boolean} position - 0 is 50% 50%, 1 is 75% 25%.
     */
    columnPosition(position)
    {
        if (position === 0) {
            this.col_left.style.width = "50%";
            this.col_right.style.width = "50%";
            this.col_2_block.style.background = "#39141E";
        }
        else if (position === 1) {
            this.col_left.style.width = "75%";
            this.col_right.style.width = "25%";
            this.col_2_block.style.background = "#173439";
        }
    }
}

// App's instantiation.
const app = new App();
app.init();