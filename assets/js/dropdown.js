
/**
 * DropDown class.
 * Manages the drop down menu.
 */

class DropDown
{
    /**
     * DropDown constructor.
     */
    constructor()
    {
        this.drop_down = document.querySelector(".drop-down");
        this.search_input = document.querySelector("input"); // Search input.
        this.setDropDown(false);
    }

    /**
     * /!\ Manages whether or not to append an element. /!\
     * Deletes the elements from the dropdown list.
     * @param {Object} result_rfc - RFC scores.
     */
    resultManager(result_rfc)
    {
        // Deletes all the elements in the drop down list.
        this.deleteEls();

        // If there one RFC or more, append the RFC.
        if (result_rfc.length > 0) {
            for (let i = 0; i < result_rfc.length; i++) {
                this.appendEl(result_rfc[i]);
            }
            // Then display the drop down list.
            this.setDropDown(true);
        }
        else {
            // Else, hide the drop down list.
            this.setDropDown(false);
        }
    }

    /**
     * Appends an element to the dropdown list.
     * @param {object} data - RFC score.
     */
    appendEl(data)
    {
        let html = '\
            <div class="dp-element" data-ref="' + data.ref + '">\
                <div class="dp-head">\
                    <p class="dp-reference">RFC' + data.ref + '</p>\
                    <p class="dp-date">' + data.date + '</p>\
                </div>\
                <p class="dp-name">' + data.name + '</p>\
            </div>\
        ';

        this.drop_down.insertAdjacentHTML("beforeend", html);
    }

    /**
     * Delete all the elements in the dropdown list.
     */
    deleteEls()
    {
        let length = this.drop_down.children.length;

        for (let i = 0; i < length; i++) {
            this.drop_down.children[0].remove();
        }
    }

    /*--- SETTERS ---*/

    /**
     * Shows or hide the drop down element.
     * @param {boolean} bool - True is displaying and false is not displaying.
     */
    setDropDown(bool)
    {
        // If it is told to be displayed and has children elements.
        if (bool && this.drop_down.children.length > 0) {
            var top = this.search_input.offsetHeight + this.search_input.offsetTop;
            var left = this.search_input.offsetLeft;

            this.drop_down.style.top = top + 1 + "px";
            this.drop_down.style.left = left + "px";

            this.drop_down.style.display = "block";
        }
        // If it is told to not be displayed.
        else {
            this.drop_down.style.display = "none";
        }
    }
}